﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Xceed.Wpf.Toolkit;

namespace LanguageAssistantManager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }

    public class Entry
    {
        public string documentName { get; set; }

        public string languageAssistantName { get; set; }

        public string assignmentDate { get; set; }

        public string deadline { get; set; }

        public string progress { get; set; }

        public string supervisorName { get; set; }

        public string remarks { get; set; }

        public string proofread { get; set; }

        public Entry(string documentName, string assignmentDate, string deadline, string supervisorName, string remarks, string LAName, string progress, string proofread)
        {
            this.documentName = documentName;
            this.assignmentDate = assignmentDate;
            this.deadline = deadline;
            this.supervisorName = supervisorName;
            this.remarks = remarks;
            this.languageAssistantName = LAName;
            this.progress = progress;
            this.proofread = proofread;
        }

        public override bool Equals(object obj)
        {
            Entry p = obj as Entry;
            if (p != null)
            {
                //can make this check case insensitive using the overload
                return (documentName + supervisorName + assignmentDate + deadline + languageAssistantName + progress + proofread).Equals(p.documentName + p.supervisorName + assignmentDate + deadline + languageAssistantName + progress + proofread);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (documentName + supervisorName + assignmentDate + deadline + languageAssistantName + progress + proofread).GetHashCode();
        }

        public override string ToString()
        {
            return documentName+"  |  "+languageAssistantName+"  |  "+assignmentDate+"  |  "+deadline+"  |  "+progress+"  |  " +supervisorName+"  |  "+remarks+"  |  "+proofread+"  |  ";
        }
    }
}
