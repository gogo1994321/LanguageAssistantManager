﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace LanguageAssistantManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        ObservableCollection<Entry> entryList = new ObservableCollection<Entry>();

        string fileName;
        public MainWindow()
        {
            InitializeComponent();
            listView.ItemsSource = entryList;
            fileName = DateTime.Today.ToShortDateString();
            assignmentDatePicker.Text = DateTime.Today.ToString();
            deadlineDatePicker.Text = DateTime.Today.ToString();
        }

        private void addNewEntryButton_Click(object sender, RoutedEventArgs e)
        {
            Entry entry = new Entry(docNameTextBlock.Text, assignmentDatePicker.Text, deadlineDatePicker.Text, supervisorNameTextBlock.Text, remarksTextBlock.Text, LATextBlock.Text, progressComboBox.SelectedItem.ToString(), proofreadComboBox.SelectedItem.ToString());
            if (!entryList.Any(c => c.GetHashCode() == entry.GetHashCode()))
            {
                entryList.Add(entry);
            }
        }

        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
            // Create save dialog 
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "Excel Files (*.xlsx)|*.xlsx";
            dlg.AddExtension = true;
            dlg.OverwritePrompt = false;
            dlg.FileName = fileName;

            //Show dialog window
            bool? result = dlg.ShowDialog();


            //Once we click save, do the exporting
            if (result == true)
            {
                XLWorkbook wb;
                IXLWorksheet ws;
                int row = 2;
                //Does this file already exists? If yes we just want to append the new rows
                if (File.Exists(dlg.FileName))
                {
                    wb = new XLWorkbook(dlg.FileName);
                    ws = wb.Worksheet(1);

                    row = ws.LastRowUsed().RowNumber()+1;
                }
                else
                {
                    wb = new XLWorkbook();
                    wb.AddWorksheet("sheet1");
                    ws = wb.Worksheet("sheet1");

                    //Add in the title row if we are making a new sheet
                    ws.Cell("A1").RichText.AddText("Task");
                    ws.Cell("B1").RichText.AddText("LA");
                    ws.Cell("C1").RichText.AddText("Tasked on");
                    ws.Cell("D1").RichText.AddText("Deadline");
                    ws.Cell("E1").RichText.AddText("Progress (Complete?)");
                    ws.Cell("F1").RichText.AddText("Tasked by");
                    ws.Cell("G1").RichText.AddText("Remarks");
                    ws.Cell("H1").RichText.AddText("Proofread");
                    ws.Rows(1, 1).Style.Font.SetBold();

                }
                   
                foreach (Entry entry in entryList)
                {
                    
                    ws.Cell("A"+row.ToString()).Value = entry.documentName;
                    ws.Cell("B" + row.ToString()).Value = entry.languageAssistantName;
                    ws.Cell("C" + row.ToString()).Value = entry.assignmentDate;
                    ws.Cell("D" + row.ToString()).Value = entry.deadline;
                    ws.Cell("E" + row.ToString()).Value = entry.progress;
                    ws.Cell("F" + row.ToString()).Value = entry.supervisorName;
                    ws.Cell("G" + row.ToString()).Value = entry.remarks;
                    ws.Cell("H" + row.ToString()).Value = entry.proofread;

                    row++;
                }
                ws.Columns().AdjustToContents();
                wb.SaveAs(dlg.FileName);

                entryList.Clear();
            }
        }

        private void progressComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            progressComboBox.ItemsSource = new List<string> { "In Progress", "Complete"};
            progressComboBox.SelectedIndex = 0;
        }

        private void proofreadComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            proofreadComboBox.ItemsSource = new List<string> { "No", "Yes" };
            proofreadComboBox.SelectedIndex = 0;
        }

        private void listView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ListView listView = sender as ListView;
            GridView gView = listView.View as GridView;

            var workingWidth = listView.ActualWidth - 35; // take into account vertical scrollbar
            var col1 = 0.125;
            var col2 = 0.125;
            var col3 = 0.125;
            var col4 = 0.125;
            var col5 = 0.125;
            var col6 = 0.125;
            var col7 = 0.125;
            var col8 = 0.125;

            gView.Columns[0].Width = workingWidth * col1;
            gView.Columns[1].Width = workingWidth * col2;
            gView.Columns[2].Width = workingWidth * col3;
            gView.Columns[3].Width = workingWidth * col4;
            gView.Columns[4].Width = workingWidth * col5;
            gView.Columns[5].Width = workingWidth * col6;
            gView.Columns[6].Width = workingWidth * col7;
            gView.Columns[7].Width = workingWidth * col8;
        }

        private void deleteEntryButton_Click(object sender, RoutedEventArgs e)
        {
            if (entryList.Count > 0)
            {
                if (listView.SelectedIndex >= 0)
                {
                    entryList.RemoveAt(listView.SelectedIndex);
                }
                else
                {
                    entryList.Remove(entryList.Last());
                }
            }
        }
    }
}
